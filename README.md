# READ ME

This is a template for writing theses at the department of computer science at the TU Dortmund.
This is a non-official document.
Even though most theses here are written in german the template is partly in english.
Goal is to provide a template usable for german and english language as well.

## Installing

You need, of course, LaTeX for using this.

The “I don’t care just install” approach:

### Ubuntu/Debian

```
sudo apt install texlive-full
```

### Fedora

```
sudo dnf install texlive-scheme-full
```

(This can take some time.)

For syntax-highlighting in code examples we use minted.
This needs `-shell-escape` as a parameter for `pdflatex`/`latexmk`.

## To Do

* Algorithms/Minted
  * Color box
  * List of listings
  * remove algorithms
  * Include code
  * Pseudocode example

* Support epub
* Add a beamer template
* Add a proposal template
